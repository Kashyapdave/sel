package TestNG_pack;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

public class Shopregikd {
	public WebDriver driver;
	 
	@BeforeTest
	  public void beforeTest() throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\LENOVO\\Desktop\\Automation\\chromedriver.exe");
		driver = new ChromeDriver();
		  driver.get("https://www.shopclues.com/");
		  driver.manage().window().maximize();
		  driver.findElement(By.xpath("//*[@id=\"sign-in\"]/a")).click();
		  Thread.sleep(5000);

		  driver.findElement(By.xpath("/html/body/div[1]/div/div/div[3]/div[1]/button[2]")).click();
		  Thread.sleep(5000);

		  driver.findElement(By.xpath("//*[@id=\"reg_tab\"]")).click();


	  }
  @Test(priority=0)
  public void Bothvalue() throws InterruptedException {
	  driver.findElement(By.xpath("//*[@id=\"register\"]/form/fieldset/div[5]/div/a")).click();
	  Thread.sleep(5000);
	  
	  String actual_message = driver.findElement(By.xpath("//*[@id=\"register\"]/form/fieldset/div[1]/div/span")).getText();
	  String expect_message = "Please enter your email id.";
	  Assert.assertEquals(actual_message, expect_message);
	  
	  String actual_message1 = driver.findElement(By.xpath("//*[@id=\"register\"]/form/fieldset/div[2]/div/span")).getText();
	  String expect_message1 = "Please enter your mobile number.";
	  Assert.assertEquals(actual_message1, expect_message1);
  }

  @Test(priority=1)
  public void Emailneg() throws InterruptedException {
	  driver.findElement(By.xpath("//*[@id=\"register\"]/form/fieldset/div[1]/input")).sendKeys("kash");
	  Thread.sleep(5000);

	  driver.findElement(By.xpath("//*[@id=\"register\"]/form/fieldset/div[5]/div/a")).click();
	  Thread.sleep(5000);
	  
	  String actual_message2 = driver.findElement(By.xpath("//*[@id=\"register\"]/form/fieldset/div[1]/div/span")).getText();
	  String expect_message2 = "Invalid email id.";
	  Assert.assertEquals(actual_message2, expect_message2);
	  
	  String actual_message3 = driver.findElement(By.xpath("//*[@id=\"register\"]/form/fieldset/div[2]/div/span")).getText();
	  String expect_message3 = "Please enter your mobile number.";
	  Assert.assertEquals(actual_message3, expect_message3);
  }
  @Test(priority=2)
  public void Emailnegalphabetical() throws InterruptedException {
	  driver.findElement(By.xpath("//*[@id=\"register\"]/form/fieldset/div[1]/input")).clear();

	  driver.findElement(By.xpath("//*[@id=\"register\"]/form/fieldset/div[1]/input")).sendKeys("!@#$");
	  driver.findElement(By.xpath("//*[@id=\"register\"]/form/fieldset/div[5]/div/a")).click();
	  Thread.sleep(5000);
	  
	  String actual_message4 = driver.findElement(By.xpath("//*[@id=\"register\"]/form/fieldset/div[1]/div/span")).getText();
	  String expect_message4 = "Invalid email id.";
	  Assert.assertEquals(actual_message4, expect_message4);
	  
	  String actual_message5 = driver.findElement(By.xpath("//*[@id=\"register\"]/form/fieldset/div[2]/div/span")).getText();
	  String expect_message5 = "Please enter your mobile number.";
	  Assert.assertEquals(actual_message5, expect_message5);
  }
  @Test(priority=3)
  public void Emailnegnum() throws InterruptedException {
	  driver.findElement(By.xpath("//*[@id=\"register\"]/form/fieldset/div[1]/input")).clear();

	  driver.findElement(By.xpath("//*[@id=\"register\"]/form/fieldset/div[1]/input")).sendKeys("12345");
	  driver.findElement(By.xpath("//*[@id=\"register\"]/form/fieldset/div[5]/div/a")).click();
	  Thread.sleep(5000);
	  String actual_message6 = driver.findElement(By.xpath("//*[@id=\"register\"]/form/fieldset/div[1]/div/span")).getText();
	  String expect_message6 = "Invalid email id.";
	  Assert.assertEquals(actual_message6, expect_message6);
	  
	  String actual_message7 = driver.findElement(By.xpath("//*[@id=\"register\"]/form/fieldset/div[2]/div/span")).getText();
	  String expect_message7 = "Please enter your mobile number.";
	  Assert.assertEquals(actual_message7, expect_message7);
  }
  @Test(priority=4)
  public void Emailnegtrim() throws InterruptedException {
	  driver.findElement(By.xpath("//*[@id=\"register\"]/form/fieldset/div[1]/input")).clear();

	  driver.findElement(By.xpath("//*[@id=\"register\"]/form/fieldset/div[1]/input")).sendKeys("   kashyapd@rbi.edu.in");
	  driver.findElement(By.xpath("//*[@id=\"register\"]/form/fieldset/div[5]/div/a")).click();
	  Thread.sleep(5000);
	    
	  String actual_message8 = driver.findElement(By.xpath("//*[@id=\"register\"]/form/fieldset/div[2]/div/span")).getText();
	  String expect_message8 = "Please enter your mobile number.";
	  Assert.assertEquals(actual_message8, expect_message8);
  }
  @Test(priority=5)
  public void Emailnegdomain() throws InterruptedException {
	  driver.findElement(By.xpath("//*[@id=\"register\"]/form/fieldset/div[1]/input")).clear();

	  driver.findElement(By.xpath("//*[@id=\"register\"]/form/fieldset/div[1]/input")).sendKeys("   kashyapd@yahoo.in");
	  driver.findElement(By.xpath("//*[@id=\"register\"]/form/fieldset/div[5]/div/a")).click();
	  Thread.sleep(5000);
	    
	  String actual_message9 = driver.findElement(By.xpath("//*[@id=\"register\"]/form/fieldset/div[2]/div/span")).getText();
	  String expect_message9 = "Please enter your mobile number.";
	  Assert.assertEquals(actual_message9, expect_message9);
  }
  @Test(priority=6)
  public void Passwordneg() throws InterruptedException {

	  driver.findElement(By.xpath("//*[@id=\"register\"]/form/fieldset/div[2]/input")).sendKeys("1234");
	  driver.findElement(By.xpath("//*[@id=\"register\"]/form/fieldset/div[5]/div/a")).click();
	  Thread.sleep(5000);
	    
	  String actual_message10 = driver.findElement(By.xpath("//*[@id=\"register\"]/form/fieldset/div[2]/div/span")).getText();
	  String expect_message10 = "Invalid mobile number.";
	  Assert.assertEquals(actual_message10, expect_message10);
  }
  @Test(priority=7)
  public void Bothdneg() throws InterruptedException {
	  driver.findElement(By.xpath("//*[@id=\"register\"]/form/fieldset/div[1]/input")).clear();
	  driver.findElement(By.xpath("//*[@id=\"register\"]/form/fieldset/div[1]/input")).sendKeys("%^&");

	  driver.findElement(By.xpath("//*[@id=\"register\"]/form/fieldset/div[2]/input")).sendKeys("1234");
	  driver.findElement(By.xpath("//*[@id=\"register\"]/form/fieldset/div[5]/div/a")).click();
	  Thread.sleep(5000);
	    
	  String actual_message11 = driver.findElement(By.xpath("//*[@id=\"register\"]/form/fieldset/div[1]/div/span")).getText();
	  String expect_message11 = "Invalid email id.";
	  Assert.assertEquals(actual_message11, expect_message11);
	  
	  String actual_message12 = driver.findElement(By.xpath("//*[@id=\"register\"]/form/fieldset/div[2]/div/span")).getText();
	  String expect_message12 = "Invalid mobile number.";
	  Assert.assertEquals(actual_message12, expect_message12);
  }
  @Test(priority=8)
  public void pwneg() throws InterruptedException {
	  driver.findElement(By.xpath("//*[@id=\"register_with_password\"]")).click();
	  Thread.sleep(5000);

	  driver.findElement(By.xpath("//*[@id=\"register\"]/form/fieldset/div[3]/input")).sendKeys("1234");
	  driver.findElement(By.xpath("//*[@id=\"register\"]/form/fieldset/div[5]/div/a")).click();
	  Thread.sleep(5000);
	    
	  String actual_message13 = driver.findElement(By.xpath("//*[@id=\"register\"]/form/fieldset/div[1]/div/span")).getText();
	  String expect_message13 = "Invalid email id.";
	  Assert.assertEquals(actual_message13, expect_message13);
	  
	  String actual_message14 = driver.findElement(By.xpath("//*[@id=\"register\"]/form/fieldset/div[2]/div/span")).getText();
	  String expect_message14 = "Invalid mobile number.";
	  Assert.assertEquals(actual_message14, expect_message14);
	  
	  String actual_message15 = driver.findElement(By.xpath("//*[@id=\"register\"]/form/fieldset/div[3]/div/span")).getText();
	  String expect_message15 = "Password must be 6 characters or more.";
	  Assert.assertEquals(actual_message15, expect_message15);
  }
  @Test(priority=9)
  public void pwneg2() throws InterruptedException {
	  driver.findElement(By.xpath("//*[@id=\"register\"]/form/fieldset/div[3]/input")).clear();

	  driver.findElement(By.xpath("//*[@id=\"register\"]/form/fieldset/div[3]/input")).sendKeys("!@#");
	  driver.findElement(By.xpath("//*[@id=\"register\"]/form/fieldset/div[5]/div/a")).click();
	  Thread.sleep(5000);
	    
	  String actual_message16 = driver.findElement(By.xpath("//*[@id=\"register\"]/form/fieldset/div[1]/div/span")).getText();
	  String expect_message16 = "Invalid email id.";
	  Assert.assertEquals(actual_message16, expect_message16);
	  
	  String actual_message17= driver.findElement(By.xpath("//*[@id=\"register\"]/form/fieldset/div[2]/div/span")).getText();
	  String expect_message17 = "Invalid mobile number.";
	  Assert.assertEquals(actual_message17, expect_message17);
	  
	  String actual_message18 = driver.findElement(By.xpath("//*[@id=\"register\"]/form/fieldset/div[3]/div/span")).getText();
	  String expect_message18 = "Password must be 6 characters or more.";
	  Assert.assertEquals(actual_message18, expect_message18);
  }
  @Test(priority=10)
  public void Registration() throws InterruptedException {
	  driver.get("https://www.shopclues.com/");
	  driver.manage().window().maximize();
	  driver.findElement(By.xpath("//*[@id=\"sign-in\"]/a")).click();
	  Thread.sleep(5000);

	  driver.findElement(By.xpath("/html/body/div[1]/div/div/div[3]/div[1]/button[2]")).click();
	  Thread.sleep(5000);

	  driver.findElement(By.xpath("//*[@id=\"reg_tab\"]")).click();
	  
	  driver.findElement(By.xpath("//*[@id=\"register\"]/form/fieldset/div[1]/input")).sendKeys("kashyapd@rbi.edu.in");
	  driver.findElement(By.xpath("//*[@id=\"register\"]/form/fieldset/div[2]/input")).sendKeys("8200739241");
	  driver.findElement(By.xpath("//*[@id=\"register_with_password\"]")).click();
	  Thread.sleep(5000);
	  driver.findElement(By.xpath("//*[@id=\"register\"]/form/fieldset/div[3]/input")).sendKeys("Kashyap@12345");
	  driver.findElement(By.xpath("//*[@id=\"register\"]/form/fieldset/div[5]/div/a")).click();

	  Thread.sleep(5000);

	  String actual_message19 = driver.findElement(By.xpath("//*[@id=\"register\"]/form/fieldset/div[1]/div/span")).getText();
	  String expect_message19 = "Account already exists with this email. Please login.";
	  Assert.assertEquals(actual_message19, expect_message19);
	  
	 
	  

  }
  @AfterTest
  public void afterTest() {
  }

}
