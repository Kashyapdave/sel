package TestNG_pack;

import org.testng.annotations.Test;
import org.testng.annotations.Test;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.AfterTest;

public class Resizablejquery {
	public WebDriver driver;

	 @BeforeTest
	  public void beforeTest() {
		 System.setProperty("webdriver.chrome.driver", "C:\\Users\\LENOVO\\Desktop\\Automation\\chromedriver.exe");
			
		  driver = new ChromeDriver();
			driver.get("https://jqueryui.com/resizable/");
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
	  }
	 
  @Test
  public void f() {
	  driver.switchTo().frame(0);
			WebElement From=driver.findElement(By.xpath("//*[@id=\"resizable\"]/div[3]"));	      			
	      Actions act=new Actions(driver);					
		
	      act.dragAndDropBy(From,135, 40).build().perform();
  }
 

  @AfterTest
  public void afterTest() {
  }

}
