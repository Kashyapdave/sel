package TestNG_pack;

import org.testng.annotations.Test;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.AfterTest;

public class Sliderjquery {
	public WebDriver driver;
	
	 @BeforeTest
	  public void beforeTest() {
		 System.setProperty("webdriver.chrome.driver", "C:\\Users\\LENOVO\\Desktop\\Automation\\chromedriver.exe");
			
		  driver = new ChromeDriver();
			driver.get("https://jqueryui.com/slider/");
			driver.manage().window().maximize();
	  }

  @Test
  public void f() {
	  driver.switchTo().frame(0);
		WebElement From=driver.findElement(By.xpath("//*[@id=\"slider\"]/span"));	      			
      Actions act=new Actions(driver);					
	
      act.dragAndDropBy(From,135, 40).build().perform();	
  }
 

  @AfterTest
  public void afterTest() {
  }

}
