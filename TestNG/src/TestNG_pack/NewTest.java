package TestNG_pack;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

public class NewTest {
	public WebDriver driver;
	
	@BeforeTest
	  public void beforeTest() {
		
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\LENOVO\\Desktop\\Automation\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.get("http://askzuma.com/");
		driver.findElement(By.xpath("/html/body/div[16]/header/div/nav/ul[2]/li[1]/a")).click();
		
	  }

	
  @Test (priority=0)
  public void Bothvalue() throws InterruptedException {
		driver.findElement(By.xpath("/html/body/div[16]/header/div/nav/div/div/form/footer/button")).click();
		Thread.sleep(5000);
		String actual_message = driver.findElement(By.xpath("/html/body/div[16]/header/div/nav/div/div/form/div[1]/div[1]/span")).getText();
		String expect_messaage = "Required";
		Assert.assertEquals(actual_message, expect_messaage); 
		
		String actual_message1 = driver.findElement(By.xpath("/html/body/div[16]/header/div/nav/div/div/form/div[1]/div[2]/span")).getText();
		String expect_messaage1 = "Required";
		Assert.assertEquals(actual_message1, expect_messaage1); 
		}

  
  
  @Test (priority=1)
  public void Email() throws InterruptedException {
	
			 driver.findElement(By.xpath("//*[@id=\"Email\"]")).sendKeys("kdv11");
			 driver.findElement(By.xpath("/html/body/div[16]/header/div/nav/div/div/form/footer/button")).click();
Thread.sleep(5000);
			 String actual_message2 = driver.findElement(By.xpath("/html/body/div[16]/header/div/nav/div/div/form/div[1]/div[1]/span")).getText();
			 String exp_message2 = "Invalid format";
			 Assert.assertEquals(actual_message2, exp_message2);
			 
			 
			 String actual_message3 = driver.findElement(By.xpath("/html/body/div[16]/header/div/nav/div/div/form/div[1]/div[2]/span")).getText();
			 String exp_message3 = "Required";
			 Assert.assertEquals(actual_message3, exp_message3);
  
  }
  
  
  @Test (priority=2)
  public void Enteremail() throws InterruptedException {
		 driver.findElement(By.xpath("//*[@id=\"Email\"]")).clear();

		 driver.findElement(By.xpath("//*[@id=\"Email\"]")).sendKeys("!@#$$$s");
		 driver.findElement(By.xpath("/html/body/div[16]/header/div/nav/div/div/form/footer/button")).click();
		 Thread.sleep(5000);

		 String actual_message4 = driver.findElement(By.xpath("/html/body/div[16]/header/div/nav/div/div/form/div[1]/div[1]/span")).getText();
		 String exp_message4 = "Invalid format";
		 Assert.assertEquals(actual_message4, exp_message4);
		 
		 String actual_message5 = driver.findElement(By.xpath("/html/body/div[16]/header/div/nav/div/div/form/div[1]/div[2]/span")).getText();
		 String exp_message5 = "Required";
		 Assert.assertEquals(actual_message5, exp_message5);
}

  @Test (priority=3)	
  public void Password() throws InterruptedException {
		 driver.findElement(By.xpath("//*[@id=\"Email\"]")).clear();

		 driver.findElement(By.xpath("//*[@id=\"Email\"]")).sendKeys("ankur1.manish@gmail.com");
		 Thread.sleep(5000);
		 driver.findElement(By.xpath("/html/body/div[16]/header/div/nav/div/div/form/footer/button")).click();

		 String actual_message6 = driver.findElement(By.xpath("/html/body/div[16]/header/div/nav/div/div/form/div[1]/div[2]/span")).getText();
		 String exp_message6 = "Required";
		 Assert.assertEquals(actual_message6, exp_message6);
		
}
  
  @Test (priority=4)	
  public void Enterpassword() throws InterruptedException {
	  driver.get("http://askzuma.com/");
		driver.findElement(By.xpath("/html/body/div[16]/header/div/nav/ul[2]/li[1]/a")).click();
		 driver.findElement(By.xpath("//*[@id=\"Email\"]")).sendKeys("ankur1.manish@gmail.com");

		driver.findElement(By.xpath("//*[@id=\"Password\"]")).sendKeys("!@##");
		 Thread.sleep(5000);
		 driver.findElement(By.xpath("/html/body/div[16]/header/div/nav/div/div/form/footer/button")).click();
		 Thread.sleep(5000);
		 String actual_message7 = driver.findElement(By.xpath("/html/body/div[12]/div/div/div/div/div/div/div[2]")).getText();
		 String exp_message7 = "Invalid email or password.";
		 Assert.assertEquals(actual_message7, exp_message7);

}
  @Test (priority=5)	
  public void Enterpassword2() throws InterruptedException {
	  driver.get("http://askzuma.com/");
		driver.findElement(By.xpath("/html/body/div[16]/header/div/nav/ul[2]/li[1]/a")).click();
		 driver.findElement(By.xpath("//*[@id=\"Email\"]")).sendKeys("        ankur1.manish@gmail.com");

		driver.findElement(By.xpath("//*[@id=\"Password\"]")).sendKeys("!@##");
		 Thread.sleep(5000);
		 driver.findElement(By.xpath("/html/body/div[16]/header/div/nav/div/div/form/footer/button")).click();
		 Thread.sleep(5000);
		 String actual_message8 = driver.findElement(By.xpath("/html/body/div[17]/header/div/nav/div/div/form/div[1]/div[1]/span")).getText();
		 String exp_message8 = "Invalid format";
		 Assert.assertEquals(actual_message8, exp_message8);

}
  @Test (priority=6)	
  public void Enterpassword3() throws InterruptedException {
	  driver.findElement(By.xpath("//*[@id=\"Email\"]")).clear();

	  driver.findElement(By.xpath("//*[@id=\"Email\"]")).sendKeys("ankur1.manish@yahoo.com");

		driver.findElement(By.xpath("//*[@id=\"Password\"]")).sendKeys("!@##");
		 Thread.sleep(5000);
		 driver.findElement(By.xpath("/html/body/div[16]/header/div/nav/div/div/form/footer/button")).click();
		 Thread.sleep(5000);
		 String actual_message9 = driver.findElement(By.xpath("/html/body/div[17]/header/div/nav/div/div/form/div[1]/div[1]/span")).getText();
		 String exp_message9 = "Invalid format";
		 Assert.assertEquals(actual_message9, exp_message9);
		 Thread.sleep(5000);
		 driver.findElement(By.xpath("/html/body/div[12]/div/div/div/div/footer/button")).click();

}
  @Test (priority=7)
  public void loginsuc() throws InterruptedException {
	  
		 driver.get("https://askzuma.com/");
	 driver.findElement(By.xpath("/html/body/div[16]/header/div/nav/ul[2]/li[1]/a")).click();

			 driver.findElement(By.xpath("//*[@id=\"Email\"]")).sendKeys("ankur1.manish@gmail.com");
			 Thread.sleep(5000);
			 driver.findElement(By.xpath("//*[@id=\"Password\"]")).sendKeys("12345678");
			 Thread.sleep(5000);
			 driver.findElement(By.xpath("//html/body/div[16]/header/div/nav/div/div/form/footer/button")).click();
			 Thread.sleep(5000);
			 driver.findElement(By.xpath("/html/body/div[16]/header/div/nav/a")).click();

			
			 
	 }
  @AfterTest
  public void afterTest() {
  }

}
