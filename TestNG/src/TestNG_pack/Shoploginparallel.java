package TestNG_pack;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.AfterTest;

public class Shoploginparallel {
	
	@BeforeTest
	  public void beforeTest() {
		
	  }
  @Test (priority=0)
  public void Login() throws InterruptedException {
	  WebDriver driver;
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\LENOVO\\Desktop\\Automation\\chromedriver.exe");
		
	  driver = new ChromeDriver();
		driver.get("https://www.shopclues.com/");
		driver.manage().window().maximize();
		driver.findElement(By.xpath("/html/body/div[1]/div/div/div[3]/div[1]/button[2]")).click();
		driver.findElement(By.xpath("//*[@id=\"sign-in\"]/a")).click();
		Thread.sleep(5000);
		driver.findElement(By.xpath("//*[@id=\"main_user_login\"]")).sendKeys("kashyapd@rbi.edu.in");
		driver.findElement(By.xpath("//*[@id=\"login\"]/form/fieldset/div[3]/input")).sendKeys("Kashyap@12345");
		driver.findElement(By.xpath("//*[@id=\"login_button\"]")).click();
		Thread.sleep(5000);
		driver.findElement(By.xpath("//*[@id=\"loginModelBox\"]/div/div[2]/div[11]/div/form/div[3]/div/a")).click();
		Actions action = new Actions(driver);
		WebElement element = driver.findElement(By.xpath("//*[@id=\"sc_uname\"]/a"));
		action.moveToElement(element).perform();
		driver.findElement(By.xpath("//*[@id=\"sc_uname\"]/div/ul/li[10]/a")).click();
  }
  
  @Test (priority=1)
  public void Loginmozila() throws InterruptedException {
	  WebDriver driver;
		System.setProperty("webdriver.gecko.driver", "C:\\Users\\LENOVO\\Desktop\\Automation\\geckodriver.exe");
		
	  driver = new FirefoxDriver();
		driver.get("https://www.shopclues.com/");
		driver.manage().window().maximize();
		Thread.sleep(5000);
		driver.findElement(By.xpath("/html/body/div[1]/div/div/div[3]/div[1]/button[2]")).click();
		driver.findElement(By.xpath("//*[@id=\"sign-in\"]/a")).click();
		Thread.sleep(5000);

		driver.findElement(By.xpath("//*[@id=\"main_user_login\"]")).sendKeys("kashyapd@rbi.edu.in");
		driver.findElement(By.xpath("//*[@id=\"login\"]/form/fieldset/div[3]/input")).sendKeys("Kashyap@12345");
		driver.findElement(By.xpath("//*[@id=\"login_button\"]")).click();
		Thread.sleep(5000);
		driver.findElement(By.xpath("//*[@id=\"loginModelBox\"]/div/div[2]/div[11]/div/form/div[3]/div/a")).click();
		driver.findElement(By.xpath("/html/body/div[1]/div/div/div[3]/div[1]/button[2]")).click();

		Actions action = new Actions(driver);
		WebElement element = driver.findElement(By.xpath("//*[@id=\"sc_uname\"]/a"));
		action.moveToElement(element).perform();
		driver.findElement(By.xpath("//*[@id=\"sc_uname\"]/div/ul/li[10]/a")).click();
  }
  @AfterTest
  public void afterTest() {
  }

}
