package TestNG_pack;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;

public class Askoginparallel {
	
	@BeforeTest
	  public void beforeTest() {

	  }
  @Test (priority=0)
  public void Login() throws InterruptedException {
	  WebDriver driver;
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\LENOVO\\Desktop\\Automation\\chromedriver.exe");
		
	  driver = new ChromeDriver();
		driver.get("https://askzuma.com/");
		driver.manage().window().maximize();
		driver.findElement(By.xpath("/html/body/div[16]/header/div/nav/ul[2]/li[1]/a")).click();
		driver.findElement(By.xpath("//*[@id=\"Email\"]")).sendKeys("ankur1.manish@gmail.com");
		driver.findElement(By.xpath("//*[@id=\"Password\"]")).sendKeys("12345678");
		driver.findElement(By.xpath("/html/body/div[16]/header/div/nav/div/div/form/footer/button")).click();

		Thread.sleep(5000);
		Thread.sleep(5000);
		driver.findElement(By.xpath("/html/body/div[16]/header/div/nav/a")).click();
  }
  
//  <listeners>
//  <listener class-name="org.uncommons.reportng.HTMLReporter"/>
//  <listener class-name="org.uncommons.reportng.JUnitXMLReporter"/>
//</listeners>

 
  @Test (priority=1)
  public void Loginmozila() throws InterruptedException {
	  WebDriver driver;
		System.setProperty("webdriver.gecko.driver", "C:\\Users\\LENOVO\\Desktop\\Automation\\geckodriver.exe");
		
	  driver = new FirefoxDriver();
		driver.get("https://askzuma.com/");
		driver.manage().window().maximize();
		driver.findElement(By.xpath("/html/body/div[16]/header/div/nav/ul[2]/li[1]/a")).click();
		driver.findElement(By.xpath("//*[@id=\"Email\"]")).sendKeys("ankur1.manish@gmail.com");
		driver.findElement(By.xpath("//*[@id=\"Password\"]")).sendKeys("12345678");
		driver.findElement(By.xpath("/html/body/div[16]/header/div/nav/div/div/form/footer/button")).click();
		Thread.sleep(5000);
		driver.findElement(By.xpath("/html/body/div[16]/header/div/nav/a")).click();

  }

  @AfterTest
  public void afterTest() {
  }

}
