package TestNG_pack;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

public class Changepwinherit extends Editprofileinheri{
  @Test (priority=9)
  public void Allvalues1() throws InterruptedException {
	  driver.navigate().refresh();
	  WebDriverWait wait2 = new WebDriverWait(driver,20);
	    wait2.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"content\"]/div[1]/div[5]/div/div/div[2]/div/div/div/div[1]/ul/li[4]/a")));
		driver.findElement(By.xpath("//*[@id=\"content\"]/div[1]/div[5]/div/div/div[2]/div/div/div/div[1]/ul/li[4]/a")).click();
		Thread.sleep(5000);
		driver.findElement(By.xpath("//*[@id=\"save_profile_but\"]")).click();
		Thread.sleep(5000);
		String actual_message10 = driver.findElement(By.xpath("//*[@id=\"passwordBlank\"]/span")).getText();
		String expect_messaage10 = "Current password cannot be empty";
		Assert.assertEquals(actual_message10, expect_messaage10); 
}
  @Test (priority=10)
  public void Currentpw() throws InterruptedException {
	  
		driver.findElement(By.xpath("//*[@id=\"passwordc\"]")).sendKeys("!@##$s");
		driver.findElement(By.xpath("//*[@id=\"save_profile_but\"]")).click();
		Thread.sleep(5000);
		String actual_message11 = driver.findElement(By.xpath("//*[@id=\"password1Blank\"]/span")).getText();
		String expect_messaage11 = "New password cannot be empty";
		Assert.assertEquals(actual_message11, expect_messaage11); 
}
  
  @Test (priority=11)
  public void Currentpw2() throws InterruptedException {
	  
		driver.findElement(By.xpath("//*[@id=\"passwordc\"]")).sendKeys("Kashyap@12345");
		driver.findElement(By.xpath("//*[@id=\"save_profile_but\"]")).click();
		Thread.sleep(5000);
		String actual_message12 = driver.findElement(By.xpath("//*[@id=\"password1Blank\"]/span")).getText();
		String expect_messaage12 = "New password cannot be empty";
		Assert.assertEquals(actual_message12, expect_messaage12); 
} 
  @Test (priority=12)
  public void Newpw() throws InterruptedException {
	  
		driver.findElement(By.xpath("//*[@id=\"password1\"]")).sendKeys("!@##");
		driver.findElement(By.xpath("//*[@id=\"save_profile_but\"]")).click();
		Thread.sleep(5000);
		String actual_message13 = driver.findElement(By.xpath("//*[@id=\"password1Blank\"]")).getText();
		String expect_messaage13 = "Password must be 6 characters or more";
		Assert.assertEquals(actual_message13, expect_messaage13); 
} 
  @Test (priority=13)
  public void Newpw2() throws InterruptedException {
		driver.findElement(By.xpath("//*[@id=\"password1\"]")).clear();

		driver.findElement(By.xpath("//*[@id=\"password1\"]")).sendKeys("1234");
		driver.findElement(By.xpath("//*[@id=\"save_profile_but\"]")).click();
		Thread.sleep(5000);
		String actual_message14 = driver.findElement(By.xpath("//*[@id=\"password1Blank\"]")).getText();
		String expect_messaage14 = "Password must be 6 characters or more";
		Assert.assertEquals(actual_message14, expect_messaage14); 
}
  @Test (priority=14)
  public void confirmNewpw() throws InterruptedException {

		driver.findElement(By.xpath("//*[@id=\"password2\"]")).sendKeys("!@##");
		driver.findElement(By.xpath("//*[@id=\"save_profile_but\"]")).click();
		Thread.sleep(5000);
		String actual_message15 = driver.findElement(By.xpath("//*[@id=\"password1Blank\"]")).getText();
		String expect_messaage15 = "Password must be 6 characters or more";
		Assert.assertEquals(actual_message15, expect_messaage15); 
}

  @Test (priority=16)
  public void Changepw() throws InterruptedException {
		driver.navigate().refresh();
		driver.findElement(By.xpath("//*[@id=\"passwordc\"]")).sendKeys("Kashyap@12345");
		Thread.sleep(5000);
		driver.findElement(By.xpath("//*[@id=\"password1\"]")).sendKeys("Kashyap@12345");

		driver.findElement(By.xpath("//*[@id=\"save_profile_but\"]")).click();
		Thread.sleep(5000);
		String actual_message17 = driver.findElement(By.xpath("//*[@id=\"password2Blank\"]/span")).getText();
		String expect_messaage17 = "Confirm password cannot be empty";
		Assert.assertEquals(actual_message17, expect_messaage17); 
}
@Test (priority=17)
public void Pwmatch5() throws InterruptedException {
	  driver.navigate().refresh();
	  driver.findElement(By.xpath("//*[@id=\"passwordc\"]")).sendKeys("Kashyap@12345");
	  
		driver.findElement(By.xpath("//*[@id=\"password1\"]")).sendKeys("Kashyap@12345");
	  	driver.findElement(By.xpath("//*[@id=\"password2\"]")).sendKeys("Kashyap@12345");
		driver.findElement(By.xpath("//*[@id=\"save_profile_but\"]")).click();
		
		Thread.sleep(5000);
		String actual_message18 = driver.findElement(By.xpath("//*[@id=\"notification_b54e527883d0e9af5c9861cd94931e52\"]/div/div[2]")).getText();
		String expect_messaage18 = "Please choose a new password";
		Assert.assertEquals(actual_message18, expect_messaage18); 
}
  @BeforeTest
  public void beforeTest() {
  }

  @AfterTest
  public void afterTest() {
  }

}
