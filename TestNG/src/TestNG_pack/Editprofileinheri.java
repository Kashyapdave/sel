package TestNG_pack;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

public class Editprofileinheri extends Sopinheri{
  
	@Test(priority=0)
  public void Updateprofile() throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(driver,40);
	    wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"firstname\"]")));
	    
		driver.findElement(By.xpath("//*[@id=\"firstname\"]")).clear();
		driver.findElement(By.xpath("//*[@id=\"save_profile_but\"]")).click();

		Thread.sleep(5000);
		String actual_message1 = driver.findElement(By.xpath("//*[@id=\"checkBlank1\"]/span")).getText();
		String expect_messaage1 = "First name cannot be blank";
		Assert.assertEquals(actual_message1, expect_messaage1); 
  }
  @Test (priority=1)
  public void Updateprofilefirst() throws InterruptedException {
		driver.findElement(By.xpath("//*[@id=\"firstname\"]")).sendKeys("!@#$");
		driver.findElement(By.xpath("//*[@id=\"save_profile_but\"]")).click();

		Thread.sleep(5000);
		String actual_message2 = driver.findElement(By.xpath("//*[@id=\"fname\"]/span")).getText();
		String expect_messaage2 = "Enter correct first name";
		Assert.assertEquals(actual_message2, expect_messaage2); 
}
@Test (priority=2)
  public void Updateprofilelast() throws InterruptedException {
	driver.findElement(By.xpath("//*[@id=\"lastname\"]")).clear();

		driver.findElement(By.xpath("//*[@id=\"lastname\"]")).sendKeys("!@#$");
		driver.findElement(By.xpath("//*[@id=\"save_profile_but\"]")).click();

		Thread.sleep(5000);
		String actual_message3 = driver.findElement(By.xpath("//*[@id=\"fname\"]/span")).getText();
		String expect_messaage3 = "Enter correct first name";
		Assert.assertEquals(actual_message3, expect_messaage3); 
}
@Test (priority=3)
  public void Updateprofilefirstpositive() throws InterruptedException {
	driver.findElement(By.xpath("//*[@id=\"firstname\"]")).clear();
	driver.findElement(By.xpath("//*[@id=\"firstname\"]")).sendKeys("Kashyap");


		driver.findElement(By.xpath("//*[@id=\"save_profile_but\"]")).click();

		Thread.sleep(5000);
		String actual_message4 = driver.findElement(By.xpath("//*[@id=\"lname\"]/span")).getText();
		String expect_messaage4 = "Enter correct last name";
		Assert.assertEquals(actual_message4, expect_messaage4); 
}
@Test (priority=4)
  public void Updateprofilemobile() throws InterruptedException {
	driver.findElement(By.xpath("//*[@id=\"phone\"]")).clear();
	driver.findElement(By.xpath("//*[@id=\"lastname\"]")).clear();

	driver.findElement(By.xpath("//*[@id=\"lastname\"]")).sendKeys("Dave");


		driver.findElement(By.xpath("//*[@id=\"save_profile_but\"]")).click();

		Thread.sleep(5000);
		String actual_message5 = driver.findElement(By.xpath("//*[@id=\"checkBlank3\"]/span")).getText();
		String expect_messaage5 = "Mobile number cannot be blank";
		Assert.assertEquals(actual_message5, expect_messaage5); 
}
@Test (priority=5)
  public void Updateprofilemobileneg() throws InterruptedException {
	driver.findElement(By.xpath("//*[@id=\"phone\"]")).sendKeys("123");


		driver.findElement(By.xpath("//*[@id=\"save_profile_but\"]")).click();

		Thread.sleep(5000);
		String actual_message6= driver.findElement(By.xpath("//*[@id=\"mnumber\"]/span")).getText();
		String expect_messaage6 = "Enter correct mobile number";
		Assert.assertEquals(actual_message6, expect_messaage6); 
}
@Test (priority=6)
  public void Updateprofilemobileneg2() throws InterruptedException {
	driver.findElement(By.xpath("//*[@id=\"phone\"]")).clear();

	driver.findElement(By.xpath("//*[@id=\"phone\"]")).sendKeys("!@#");


		driver.findElement(By.xpath("//*[@id=\"save_profile_but\"]")).click();

		Thread.sleep(5000);
		String actual_message7 = driver.findElement(By.xpath("//*[@id=\"mnumber\"]/span")).getText();
		String expect_messaage7 = "Enter correct mobile number";
		Assert.assertEquals(actual_message7, expect_messaage7); 
}
@Test (priority=7)
  public void Updateprofilemobileneg3() throws InterruptedException {
	driver.findElement(By.xpath("//*[@id=\"phone\"]")).clear();

	driver.findElement(By.xpath("//*[@id=\"phone\"]")).sendKeys("dasfaf");


		driver.findElement(By.xpath("//*[@id=\"save_profile_but\"]")).click();

		Thread.sleep(5000);
		String actual_message8 = driver.findElement(By.xpath("//*[@id=\"mnumber\"]/span")).getText();
		String expect_messaage8 = "Enter correct mobile number";
		Assert.assertEquals(actual_message8, expect_messaage8); 
}
@Test (priority=8)
  public void Updateprofilemobilepositive() throws InterruptedException {
	driver.findElement(By.xpath("//*[@id=\"phone\"]")).clear();

	driver.findElement(By.xpath("//*[@id=\"phone\"]")).sendKeys("7383803552");

	driver.findElement(By.xpath("//*[@id=\"content\"]/div[1]/div[5]/div/div/div[2]/div/div/div/div[2]/div[2]/form/fieldset/div[3]/ul/li[2]/label")).click();

		driver.findElement(By.xpath("//*[@id=\"save_profile_but\"]")).click();

		Thread.sleep(5000);
		String actual_message9 = driver.findElement(By.xpath("//*[@id=\"notification_85214fdf7d646b0d64eeb251383996d0\"]/div/div[2]")).getText();
		String expect_messaage9 = "Profile has been updated successfully.";
		Assert.assertEquals(actual_message9, expect_messaage9); 

}
  @BeforeTest
  public void beforeTest() {
  }

  @AfterTest
  public void afterTest() {
  }

}
