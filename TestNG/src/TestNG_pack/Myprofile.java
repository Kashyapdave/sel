package TestNG_pack;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

public class Myprofile {
	public WebDriver driver;
	
	@BeforeTest
	  public void beforeTest() throws InterruptedException {

		System.setProperty("webdriver.chrome.driver", "C:\\Users\\LENOVO\\Desktop\\Automation\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.get("https://www.shopclues.com/");
		driver.manage().window().maximize();
		driver.findElement(By.xpath("//*[@id=\"sign-in\"]/a")).click();

		Thread.sleep(5000);	


	}
		@Test (priority=0)
		  public void Login() throws InterruptedException {
			driver.findElement(By.xpath("/html/body/div[1]/div/div/div[3]/div[1]/button[2]")).click();

		driver.findElement(By.xpath("//*[@id=\"main_user_login\"]")).sendKeys("kashyapd@rbi.edu.in");
		driver.findElement(By.xpath("//*[@id=\"login\"]/form/fieldset/div[3]/input")).sendKeys("Kashyap@12345");
		driver.findElement(By.xpath("//*[@id=\"login_button\"]")).click();
		Thread.sleep(5000);
		driver.findElement(By.xpath("//*[@id=\"loginModelBox\"]/div/div[2]/div[11]/div/form/div[3]/div/a")).click();
		Thread.sleep(5000);

		Actions action = new Actions(driver);
		WebElement element = driver.findElement(By.xpath("//*[@id=\"sc_uname\"]/a"));
		action.moveToElement(element).perform();
		driver.findElement(By.xpath("//*[@id=\"sc_uname\"]/div/ul/li[4]/a")).click();
		
 }
		@Test (priority=1)
		  public void Bothvalue() throws InterruptedException {
			driver.findElement(By.xpath("//*[@id=\"lastname\"]")).clear();

			driver.findElement(By.xpath("//*[@id=\"save_profile_but\"]")).click();
			Thread.sleep(5000);
				String actual_message = driver.findElement(By.xpath("//*[@id=\"checkBlank2\"]/span")).getText();
				String expect_messaage = "Last name cannot be blank";
				Assert.assertEquals(actual_message, expect_messaage); 
				
				}	
		@Test (priority=2)
		  public void Lastname() throws InterruptedException {
			driver.findElement(By.xpath("//*[@id=\"lastname\"]")).clear();
			driver.findElement(By.xpath("//*[@id=\"lastname\"]")).sendKeys("!@##");

			driver.findElement(By.xpath("//*[@id=\"save_profile_but\"]")).click();
			Thread.sleep(5000);
				String actual_message1 = driver.findElement(By.xpath("//*[@id=\"lname\"]/span")).getText();
				String expect_messaage1 = "Enter correct last name";
				Assert.assertEquals(actual_message1, expect_messaage1); 
				
				}	
		@Test (priority=3)
		  public void Lastname2() throws InterruptedException {
			driver.findElement(By.xpath("//*[@id=\"lastname\"]")).clear();
			driver.findElement(By.xpath("//*[@id=\"lastname\"]")).sendKeys("12345");

			driver.findElement(By.xpath("//*[@id=\"save_profile_but\"]")).click();
			Thread.sleep(5000);
				String actual_message2= driver.findElement(By.xpath("//*[@id=\"lname\"]/span")).getText();
				String expect_messaage2 = "Enter correct last name";
				Assert.assertEquals(actual_message2, expect_messaage2); 
				
				}
		@Test (priority=4)
		  public void Firstname() throws InterruptedException {
			driver.findElement(By.xpath("//*[@id=\"firstname\"]")).clear();
			driver.findElement(By.xpath("//*[@id=\"firstname\"]")).sendKeys("12345");

			driver.findElement(By.xpath("//*[@id=\"save_profile_but\"]")).click();
			Thread.sleep(5000);
				String actual_message3= driver.findElement(By.xpath("//*[@id=\"fname\"]/span")).getText();
				String expect_messaage3 = "Enter correct first name";
				Assert.assertEquals(actual_message3, expect_messaage3); 
				
				}
		@Test (priority=5)
		  public void Firstname2() throws InterruptedException {
			driver.findElement(By.xpath("//*[@id=\"firstname\"]")).clear();
			driver.findElement(By.xpath("//*[@id=\"firstname\"]")).sendKeys("!@##");

			driver.findElement(By.xpath("//*[@id=\"save_profile_but\"]")).click();
			Thread.sleep(5000);
				String actual_message4= driver.findElement(By.xpath("//*[@id=\"fname\"]/span")).getText();
				String expect_messaage4 = "Enter correct first name";
				Assert.assertEquals(actual_message4, expect_messaage4); 
				
				}
		
		@Test (priority=6)
		  public void Mobile() throws InterruptedException {
			driver.findElement(By.xpath("//*[@id=\"phone\"]")).clear();
			driver.findElement(By.xpath("//*[@id=\"phone\"]")).sendKeys("!@##");

			driver.findElement(By.xpath("//*[@id=\"save_profile_but\"]")).click();
			Thread.sleep(5000);
				String actual_message5= driver.findElement(By.xpath("//*[@id=\"fname\"]/span")).getText();
				String expect_messaage5 = "Enter correct first name";
				Assert.assertEquals(actual_message5, expect_messaage5); 
				
				}
		@Test (priority=7)
		  public void Mobile2() throws InterruptedException {
			driver.findElement(By.xpath("//*[@id=\"phone\"]")).clear();
			driver.findElement(By.xpath("//*[@id=\"phone\"]")).sendKeys("1234");

			driver.findElement(By.xpath("//*[@id=\"save_profile_but\"]")).click();
			Thread.sleep(5000);
				String actual_message6= driver.findElement(By.xpath("//*[@id=\"fname\"]/span")).getText();
				String expect_messaage6 = "Enter correct first name";
				Assert.assertEquals(actual_message6, expect_messaage6); 

				
				}
		@Test (priority=8)
		  public void Mobile3() throws InterruptedException {
			driver.findElement(By.xpath("//*[@id=\"content\"]/div[1]/div[5]/div/div/div[2]/div/div/div/div[2]/div[2]/form/fieldset/div[3]/ul/li[2]/label")).click();

		

			driver.findElement(By.xpath("//*[@id=\"save_profile_but\"]")).click();
			Thread.sleep(5000);
				String actual_message7= driver.findElement(By.xpath("//*[@id=\"fname\"]/span")).getText();
				String expect_messaage7 = "Enter correct first name";
				Assert.assertEquals(actual_message7, expect_messaage7); 
				
				}
		@Test (priority=9)
		  public void Update() throws InterruptedException {
			driver.navigate().refresh();
			driver.findElement(By.xpath("//*[@id=\"save_profile_but\"]")).click();


			Thread.sleep(5000);
				String actual_message8= driver.findElement(By.xpath("//*[@id=\"notification_85214fdf7d646b0d64eeb251383996d0\"]/div/div[2]")).getText();
				String expect_messaage8 = "Profile has been updated successfully.";
				Assert.assertEquals(actual_message8, expect_messaage8); 
				
				}
		
		
  @AfterTest
  public void afterTest() {
  }

}
