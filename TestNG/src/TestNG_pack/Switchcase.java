package TestNG_pack;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class Switchcase {

	public WebDriver driver;
	
	
	@BeforeTest
	public void beforeTest() {
		
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\LENOVO\\Desktop\\Automation\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.get("http://demo.guru99.com/selenium/deprecated.html");
		driver.manage().window().maximize();
	}
	
	@Test
	public void Switchcase() {
		
		int iSwitch=4;
		switch(iSwitch) {
		
		case 0:
		System.out.println("Zero");
		break;
		
		case 1:
			System.out.println("One");
			break;
			
		case 2:
			System.out.println("Two");
			break;
			
		case 3:
			System.out.println("Three");
			break;
			
		case 4:
			System.out.println("Four");
			break;
			
		default:
			System.out.println("Not in the List");
			break;
		
		}
	}
	
	@AfterTest
	public void aftertest() {
		
		driver.quit();
	}
}
