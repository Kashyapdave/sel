package TestNG_pack;

import org.testng.annotations.Test;

import org.testng.annotations.BeforeTest;



import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.AfterTest;

public class Dragjquery {
	public WebDriver driver;
	@BeforeTest
	
	  public void beforeTest() throws InterruptedException  {
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\LENOVO\\Desktop\\Automation\\chromedriver.exe");
		
		  driver = new ChromeDriver();
			driver.get("https://jqueryui.com/draggable/");
			driver.manage().window().maximize();
			Thread.sleep(5000);
			
	  }
	
  @Test
  public void f() {
	  driver.switchTo().frame(0);
	  Actions builder = new Actions(driver);
	  
	  WebElement from = driver.findElement(By.id("draggable"));
	  
	  WebElement to = driver.findElement(By.id("draggable")); 
	  //Perform drag and drop
	  builder.dragAndDrop(from, to).perform();
	  
	  String textTo = to.getText();
	  
	  if(textTo.equals("Dropped!")) {
	  System.out.println("PASS: Source is dropped to target as expected");
	  }else {
	  System.out.println("FAIL: Source couldn't be dropped to target as expected");
	  }
	  
  }
  

  @AfterTest
  public void afterTest() {
  }

}
