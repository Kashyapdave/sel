package TestNG_pack;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.AfterTest;

public class Iframejquery {
	public WebDriver driver;
	
	 @BeforeTest
	  public void beforeTest() { 
		 System.setProperty("webdriver.chrome.driver", "C:\\Users\\LENOVO\\Desktop\\Automation\\chromedriver.exe");
		
	  driver = new ChromeDriver();
		driver.get("https://jqueryui.com");
		driver.manage().window().maximize();
	  }
  @Test
  public void f() throws InterruptedException {
	  driver.switchTo().frame(0);
		WebElement ele=driver.findElement(By.xpath("//*[@id=\"content\"]/iframe"));	      			
		Actions act=new Actions(driver);	
		Thread.sleep(5000);
		driver.findElement(By.xpath("//*[@id=\"menu-top\"]/li[6]/a")).click();
  }
 

  @AfterTest
  public void afterTest() {
  }

}
