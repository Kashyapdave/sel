package TestNG_pack;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.AfterTest;

public class Sortablejquery {
	public WebDriver driver;

	
	 @BeforeTest
	  public void beforeTest() throws InterruptedException {
		 System.setProperty("webdriver.chrome.driver", "C:\\Users\\LENOVO\\Desktop\\Automation\\chromedriver.exe");
			
		  driver = new ChromeDriver();
			driver.get("https://jqueryui.com/sortable/");
			driver.manage().window().maximize();
			Thread.sleep(5000);
			driver.switchTo().frame(0);
	  }

	 
  @Test(priority=0)
  public void f() throws InterruptedException {
	  WebElement e1= driver.findElement(By.xpath("//*[@id=\"sortable\"]/li[1]"));

	  Actions act=new Actions(driver);

	  org.openqa.selenium.interactions.Action action = act.dragAndDropBy(e1, 70,90).build();

	  action.perform();
	  Thread.sleep(5000);
  }
  
  @Test (priority = 1)
  public void f1() throws InterruptedException {


 WebElement e2= driver.findElement(By.xpath("//*[@id=\"sortable\"]/li[4]"));

 Actions act=new Actions(driver);

 org.openqa.selenium.interactions.Action action = act.dragAndDropBy(e2, 90,70).build();

 action.perform();
 Thread.sleep(5000);
  }
  @AfterTest
  public void afterTest() {
  }

}
