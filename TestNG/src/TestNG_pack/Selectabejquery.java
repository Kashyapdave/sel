package TestNG_pack;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.AfterTest;

public class Selectabejquery {
	public WebDriver driver;
	
	@BeforeTest
	  public void beforeTest() throws InterruptedException {
		 System.setProperty("webdriver.chrome.driver", "C:\\Users\\LENOVO\\Desktop\\Automation\\chromedriver.exe");
			
		  driver = new ChromeDriver();
			driver.get("https://jqueryui.com/selectable/");
			driver.manage().window().maximize();
			Thread.sleep(5000);
	  }

  @Test
  public void f() {
	  driver.switchTo().frame(0); //need to switch to this frame before clicking the slider
		
		
		Actions action=new Actions(driver);
		action.keyDown(Keys.CONTROL).build().perform();
		driver.findElement(By.xpath(".//*[@id='selectable']/li[1]")).click();
		driver.findElement(By.xpath(".//*[@id='selectable']/li[2]")).click();
		driver.findElement(By.xpath(".//*[@id='selectable']/li[3]")).click();
		action.keyUp(Keys.CONTROL).build().perform();

		
		//driver.findElement(By.xpath("//*[@id=\"selectable\"]/li[1]")).click();
		//driver.findElement(By.xpath("//*[@id=\"selectable\"]/li[2]")).click();
	
		
		
  }
 
  @AfterTest
  public void afterTest() {
  }

}
